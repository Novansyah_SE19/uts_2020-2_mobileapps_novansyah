import React, { useEffect } from 'react';
import { createStackNavigator} from '@react-navigation/stack';
import { NavigationContainer,useNavigation } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Icon from 'react-native-vector-icons/Ionicons';
import AuthLoadingScreen from './screens/AuthLoadingScreen';
import SplashScreen from './screens/SplashScreen';
import RegisterScreen from './screens/RegisterScreen';
import WelcomeScreen from './screens/WelcomeScreen';
import OtpScreen from './screens/OtpScreen';
import HomeScreen from './screens/HomeScreen';
import ProfilScreen from './screens/ProfilScreen';
import ChatScreen from './screens/ChatScreen';
import StartScreen from './screens/StartScreen';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

const BottomScreen = ({navigation})=>(
  <Tab.Navigator
    tabBarOptions={{
        activeTintColor:'#37A936',
        labelStyle:{fontSize:16},
        style:{height:60}
    }}
    
  >
    <Tab.Screen 
          name="Home" 
          component={HomeScreen} 
          options={{
              tabBarLabel:"Chats",
              tabBarIcon:({ color, size }) => (
                <Icon name="chatbubble-ellipses-outline" color={color} size={40} />
              ),
          }}
    />
    <Tab.Screen 
          name="Profil" 
          component={ProfilScreen} 
          options={{
              tabBarLabel:'Menu',
              tabBarIcon:({color,size})=>(
                <Icon name="menu-outline" color={color} size={40}/>
              )
          }}
    />
  </Tab.Navigator>
);

export default Router = () =>{
  return(
    <NavigationContainer>
        
        <Stack.Navigator >
          <Stack.Screen 
              name="AuthLoadingScreen" 
              component={AuthLoadingScreen} 
              options={{
                  headerShown:false,
              }}
          />
          <Stack.Screen 
              name="SplashScreen" 
              component={SplashScreen} 
              options={{
                  headerShown:false,
              }}
          />
          <Stack.Screen 
              name="WelcomeScreen" 
              component={WelcomeScreen} 
              options={{
                  headerShown:false,
              }}
          />
          <Stack.Screen 
              name="RegisterScreen" 
              component={RegisterScreen} 
              options={{
                  headerShown:false,
              }}
          />
          <Stack.Screen 
              name="OtpScreen" 
              component={OtpScreen} 
              options={{
                  headerShown:false,
              }}
          />
          <Stack.Screen 
              name="HomeScreen" 
              component={BottomScreen} 
              options={{
                  headerShown:false,
              }}
          />
          
          <Stack.Screen 
              name="StartScreen" 
              component={StartScreen} 
              options={{
                  headerShown:false,
              }}
          />
          <Stack.Screen 
              name="ChatScreen" 
              component={ChatScreen} 
              options={({ route }) => ({
                title: route.params.thread.name
              })}
          />
        </Stack.Navigator>
        
    </NavigationContainer>
  );
  
  }