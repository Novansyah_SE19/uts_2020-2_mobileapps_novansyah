import React, { Component, useEffect } from 'react';
import { ActivityIndicator, Image, StatusBar, Text, View } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';

export default class AuthLoadingScreen extends Component{

    async componentDidMount(){
        
        const data = await AsyncStorage.getItem('data');
        const uid = JSON.parse(data);
        // console.log(uid.user.uid)
        setTimeout(() => {
            const {navigation} = this.props;
            
            if(uid != null){
                console.log(uid.user.uid)
                navigation.replace('HomeScreen');
            }
            else{
                navigation.replace('SplashScreen');
            }
        }, 5000);
    }
    render(){
        return(
            <View style={{flex:1,backgroundColor:'#37A936'}}>
                <ActivityIndicator/>
                <StatusBar barStyle="default"></StatusBar>

                <View style={{flex:1,marginTop:250,alignItems:'center'}}>
                    <Image source={require('../img/logo.png')} style={{width:200,height:172}}/>
                    <Text style={{fontSize:25,color:'white',fontWeight:'bold',marginTop:10}}>from Hello Kreasi</Text>
                </View>
            </View>
        )
    }
}