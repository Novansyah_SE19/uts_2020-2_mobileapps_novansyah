import React, { Component, useEffect } from 'react';
import { ActivityIndicator, Image, Linking, StatusBar, Text, TouchableOpacity, View } from 'react-native';

export default class WelcomeScreen extends Component{

    handlePress(){
        Linking.openURL('http://hiapp.id/privacy#')
    }
    render(){
        const {navigation} = this.props;
        return(
            <View style={{flex:1,backgroundColor:'white'}}>
                <ActivityIndicator/>
                <StatusBar barStyle="default"></StatusBar>

                <View style={{flexDirection:'row',alignItems:'center',marginTop:50, justifyContent:'center'}}>
                    <Text style={{fontWeight:'bold',fontSize:20}}>Welcome to </Text>
                    <Text style={{color:'#37A936',fontWeight:'bold',fontSize:16}}>Hi App !</Text>
                </View>
                <View style={{flex:1,alignItems:'center',backgroundColor:'white'}} >
                    <Image source={require('../img/logohiapp2.jpg')} style={{marginTop:150, width:280,height:210}}/>
                </View>

                <View style={{alignItems:'center',marginBottom:100}}>
                    <Text>By Signing up, you agree to the</Text>
                    <Text style={{color:'#37A936'}} onPress={()=>this.handlePress()}>Privacy and Terms of Service</Text>
                </View>
                <View style={{alignItems:'center'}}>
                    <TouchableOpacity   onPress={()=>navigation.navigate('RegisterScreen')}
                                        style={{width:'90%',
                                            backgroundColor:'#37A936',
                                            height:40,marginBottom:50,
                                            borderRadius:20,alignItems:'center',justifyContent:'center'}}>
                        <Text style={{color:'white',fontSize:16}}>Get Started</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}