import React, { Component, useEffect } from 'react';
import { ActivityIndicator, Image, KeyboardAvoidingView, StatusBar, Text, TextInput, View } from 'react-native';
import { Switch } from 'react-native-paper';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/Ionicons';
import AsyncStorage from '@react-native-async-storage/async-storage';

export default class ProfilScreen extends Component{
    state={
        first:'',
        switch:false,
        nama:'',
    }

    handlePress(){
        
        const {navigation} = this.props;
        navigation.navigate('HomeScreen')
    }

    async componentDidMount(){
        const nama = await AsyncStorage.getItem('channel');
        this.setState({nama:nama})
    }
    render(){
        return(
            <View style={{flex:1}}>
                {/* <ActivityIndicator/> */}
                <StatusBar barStyle="default"></StatusBar>

                <View style={{backgroundColor:'white',paddingVertical:20}}>
                    <Text style={{fontSize:30}}>Menu</Text>
                    <TouchableOpacity style={{height:50,marginTop:30,justifyContent:'space-between',flexDirection:'row'}}>
                        <View style={{flexDirection:'row',marginHorizontal:10}}>
                        <View style={{backgroundColor:'white',width:50,borderRadius:25}}>
                            <Image source={require('../img/user.png')} style={{width:50,height:50}}></Image>
                            
                        </View>
                        <View style={{justifyContent:'center',marginHorizontal:10}}>
                            <Text style={{fontSize:20}}>{this.state.nama}</Text>
                        </View>
                        </View>
                        <View>
                            <Image source={require('../img/arrow-right.png')} style={{width:50,height:50,tintColor:'#37A936'}}/>
                        </View>
                    </TouchableOpacity>
                    <View style={{height:50,marginTop:30,justifyContent:'space-between',flexDirection:'row'}}>
                        <Text style={{fontSize:20,marginHorizontal:10,alignSelf:'center'}}>Enable Notification</Text>
                        <Switch value={this.state.switch} 
                                onValueChange={()=>this.setState({switch:!this.state.switch})} 
                                style={{marginHorizontal:10}}
                                color="#37A936"/>
                    </View>
                </View>
                <View style={{flexDirection:'row',justifyContent:'space-between',marginHorizontal:30,marginVertical:20}}>
                    <TouchableOpacity style={{width:100,height:80,backgroundColor:'#1fbc1a',borderRadius:10,justifyContent:'center'}}>
                        <Image source={require('../img/user-check.png')} style={{width:30,height:30, alignSelf:'center'}}/>
                        <Text style={{alignSelf:'center',color:'white'}}>Account</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{width:100,height:80,backgroundColor:'orange',borderRadius:10,justifyContent:'center'}}>
                        <Image source={require('../img/rate.png')} style={{width:30,height:30, alignSelf:'center'}}/>
                        <Text style={{alignSelf:'center',color:'white'}}>Rate</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{width:100,height:80,backgroundColor:'red',borderRadius:10,justifyContent:'center'}}>
                        <Image source={require('../img/user-inv.png')} style={{width:30,height:30, alignSelf:'center'}}/>
                        <Text style={{alignSelf:'center',color:'white'}}>Invite</Text>
                    </TouchableOpacity>
                </View>
                <View style={{flexDirection:'row',justifyContent:'space-between',marginHorizontal:30,marginVertical:20}}>
                    <TouchableOpacity style={{width:100,height:80,backgroundColor:'#629dea',borderRadius:10,justifyContent:'center'}}>
                        <Image source={require('../img/setting.png')} style={{width:30,height:30, alignSelf:'center'}}/>
                        <Text style={{alignSelf:'center',color:'white'}}>Account</Text>
                    </TouchableOpacity>
                </View>

            </View>
        )
    }
}