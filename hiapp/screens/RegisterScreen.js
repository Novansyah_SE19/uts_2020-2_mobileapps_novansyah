import React, { Component, useEffect } from 'react';
import { ActivityIndicator, Image, LogBox, StatusBar, Text, TextInput, View } from 'react-native';
import { Input } from 'react-native-elements';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/Ionicons';
import SendSMS from 'react-native-sms'
import Firebase from '@react-native-firebase/app';
import auth from '@react-native-firebase/auth';

export default class RegisterScreen extends Component{

    state={
        number:'',
        confirm:null,
        code:'',
    }
    handlePress(confirmation){
        
        const {navigation} = this.props;
        navigation.navigate('OtpScreen',{confirm:this.state.confirm,number:this.state.number})
      
    }
    componentDidMount(){
        LogBox.ignoreLogs(['Warn ...']);
        LogBox.ignoreLogs(['Warning: ...']);
        console.ignoredYellowBox = true;
        LogBox.ignoreAllLogs();
        
        var firebaseConfig = {
            apiKey: "AIzaSyAInKpe54qfZXog2JyFQXaisRwQOg0bpn8",
            authDomain: "hiapp-4d313.firebaseapp.com",
            projectId: "hiapp-4d313",
            storageBucket: "hiapp-4d313.appspot.com",
            messagingSenderId: "366732866914",
            appId: "1:366732866914:web:44748fd544bdc53f73a83b",
            measurementId: "G-V5S0Q5WZCG"

          };

          if(!Firebase.apps.length){
            Firebase.initializeApp(firebaseConfig);
          }
          else{
              Firebase.app();
          }
    }

    SignIn= async (phoneNumber)=>{
        console.log(this.state.number)
        const {navigation} = this.props;
        const confirmation = await auth().signInWithPhoneNumber("+62"+this.state.number);
        const id = await Firebase.auth().currentUser;
        this.setState({confirm:confirmation})
        console.log(confirmation)
        console.log("current "+id)

        // auth().onAuthStateChanged(function(user) {
        //     if (user) {
              
        //         console.log("current2 "+user)
        //     } else {
        //       // No user is signed in.
        //     }
        //   });
        // await AsyncStorage.setItem('data',JSON.stringify(id))
        this.handlePress(confirmation);
    }
  
    render(){
        return(
            <View style={{flex:1,backgroundColor:'white'}}>
                <ActivityIndicator/>
                <StatusBar barStyle="default"></StatusBar>

                <View style={{alignItems:'center',marginTop:50, justifyContent:'center'}}>
                    <Text style={{fontWeight:'bold',fontSize:16}}>Verify Your Mobile Number</Text>
                    <Text style={{fontSize:16,marginTop:10}}>Enter Your phone number, so we will send you</Text>
                    <Text style={{fontWeight:'bold',fontSize:16}}>One Time Password</Text>
                </View>

                <View style={{flex:1,alignItems:'center',backgroundColor:'white'}}>
                <View style={{width:'70%',marginTop:80}}>
                <Input
                    placeholder='INPUT WITH CUSTOM ICON'
                    value="Indonesia (+62)"
                    editable={false}
                    leftIcon={
                        <Image source={require('../img/flag.png')} style={{width:30,height:20,borderColor:'black',borderWidth:1}}></Image>
                    }
                />
                <Input
                    placeholder='Mobile Number'
                    keyboardType="numeric"
                    onChangeText={(text)=>this.setState({number:text})}
                    leftIcon={
                        <Icon
                        name='phone-portrait-outline'
                        size={24}
                        color='black'
                        style={{width:30,height:30}}
                        />
                    }
                />
                </View>

                </View>

                <View style={{alignItems:'center',flex:1}}>
                    <TouchableOpacity   onPress={()=>this.SignIn('')}
                                        style={{paddingHorizontal:100,
                                            backgroundColor:'#37A936',
                                            height:40,marginBottom:50,
                                            borderRadius:20,alignItems:'center',justifyContent:'center'}}>
                        <Text style={{color:'white',fontSize:16}}>Get OTP</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}