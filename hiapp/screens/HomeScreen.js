import React, { Component, useEffect } from 'react';
import { ActivityIndicator, FlatList, Image, StyleSheet, StatusBar, Text, TextInput, View, TouchableOpacity } from 'react-native';
import { Overlay, Input } from 'react-native-elements';
import { Searchbar } from 'react-native-paper';
import Icon from 'react-native-vector-icons/Ionicons';
import firestore from '@react-native-firebase/firestore';
import AsyncStorage from '@react-native-async-storage/async-storage';

export default class HomeScreen extends Component{

    state={
        roomName:'test',
        search:'',
        thread:'',
        loading:false,
        name:'',
        overlay:false,
    }

    handlePress(){
        
        const {navigation} = this.props;
        navigation.navigate('ChatScreen')
    }

    handleButtonPress() {
        const {navigation} = this.props;
        this.setState({overlay:false})
        if (this.state.roomName.length > 0) {
          // create new thread using firebase & firestore
          firestore()
            .collection(this.state.name)
            .add({
              name: this.state.roomName,
              latestMessage: {
                text: `${this.state.roomName} created. Welcome!`,
                createdAt: new Date().getTime()
              }
            })
            .then(() => {
            //   navigation.replace('HomeScreen')
            })
        }
      }

    async componentDidMount(){
        const channel = await AsyncStorage.getItem('channel');
        console.log(channel)
        this.setState({name:channel})

        firestore()
            .collection(channel)
            .orderBy('latestMessage.createdAt', 'desc')
            .onSnapshot(querySnapshot => {
                const threads = querySnapshot.docs.map(documentSnapshot => {
                return {
                    _id: documentSnapshot.id,
                    name: '',
                    latestMessage: { text: '' },
                    ...documentSnapshot.data()
                }
                })

                this.setState({thread:(threads)})
                console.log(threads.length)
                if(threads.length<1){
                    this.setState({loading:true})
                }
                
            })
        
        return () => unsubscribe()
    }

    render(){
        const {navigation} = this.props;
        
        return(
            <View style={{flex:1,backgroundColor:'white'}}>
                <ActivityIndicator/>
                <StatusBar barStyle="default"></StatusBar>

                <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                    <Text style={{fontSize:30}}>Chats</Text>
                    <Icon name="add" size={40} color="#37A936" onPress={()=>this.setState({overlay:true})}/>
                </View>
                <View style={{marginTop:30,alignItems:'center'}}>
                    <Searchbar
                        placeholder="Type Here..."
                        onChangeText={(text)=>this.setState({search:text})}
                        value={this.state.search}
                        inputStyle={{justifyContent:'center'}}
                        style={{width:'90%',borderWidth:0,borderRadius:20,backgroundColor:'grey',height:40}}
                        icon={()=>
                            <Icon name="search" size={25}/>
                        
                        }
                    />
                </View>
                
                {
                    this.state.loading ? (
                    <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                            <Image source={require('../img/notfound2.png')} style={{width:250,height:250}}></Image>
                    </View>
                    ):(
                        <FlatList
                            style={{marginTop:50}}
                            data={this.state.thread}
                            keyExtractor={item => item._id}
                            renderItem={({ item }) => (
                            <TouchableOpacity onPress={() => navigation.navigate('ChatScreen', { thread: item , name: this.state.name})}
                                        style={{
                                            borderBottomWidth:1,
                                            borderBottomColor:'#37A936'
                                        }}
                            >
                                <View style={[styles.row,{marginHorizontal:10}]}>
                                <View style={styles.content}>
                                    <View style={styles.header}>
                                    <Text style={styles.nameText}>{item.name}</Text>
                                    </View>
                                    <Text style={styles.contentText}>
                                    {item.latestMessage.text.slice(0, 90)}
                                    </Text>
                                </View>
                                </View>
                            </TouchableOpacity>
                            )}
                            // ItemSeparatorComponent={() => <Separator />}
                        />
                    )
                }

                <View>
                     <Overlay isVisible={this.state.overlay} onBackdropPress={()=>this.setState({overlay:!this.state.overlay})}>
                        
                        <View style={{width:'70%'}}>
                        <TextInput
                            placeholder='INPUT ROOM NAME'
                            onChangeText={(text)=>this.setState({roomName:text})}
                            // style={{width}}
                        />
                        
                        </View>
                            <TouchableOpacity   onPress={()=>this.handleButtonPress()}
                                                style={{paddingHorizontal:100,
                                                    backgroundColor:'#37A936',
                                                    height:40,marginBottom:50,
                                                    borderRadius:20,alignItems:'center',justifyContent:'center'}}>
                                <Text style={{color:'white',fontSize:16}}>Create</Text>
                            </TouchableOpacity>
                    </Overlay>
                </View>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#dee2eb'
    },
    title: {
      marginTop: 20,
      marginBottom: 30,
      fontSize: 28,
      fontWeight: '500'
    },
    row: {
      paddingRight: 10,
      paddingLeft: 5,
      paddingVertical: 5,
      flexDirection: 'row',
      alignItems: 'center'
    },
    content: {
      flexShrink: 1
    },
    header: {
      flexDirection: 'row'
    },
    nameText: {
      fontWeight: '600',
      fontSize: 18,
      color: '#000'
    },
    dateText: {},
    contentText: {
      color: '#949494',
      fontSize: 16,
      marginTop: 2
    }
  })