import React, { Component, useEffect } from 'react';
import { ActivityIndicator, Image, Linking, StatusBar, Text, TouchableOpacity, View } from 'react-native';
import { Input } from 'react-native-elements';
import AsyncStorage from '@react-native-async-storage/async-storage';


export default class StartScreen extends Component{

    state={
        name:''
    }

    async handlePress(){
        
        const {navigation} = this.props;
        console.log(this.state.name)
        await AsyncStorage.setItem('channel',this.state.name)
        navigation.navigate('HomeScreen')
    }
   
    render(){
        const {navigation} = this.props;
        return(
            <View style={{flex:1,backgroundColor:'white'}}>
                <ActivityIndicator/>
                <StatusBar barStyle="default"></StatusBar>

                
                <View style={{alignItems:'center',marginTop:50, justifyContent:'center'}}>
                    <Text style={{fontWeight:'bold',fontSize:16}}>Here we go!</Text>
                    <Text style={{fontSize:16,marginTop:10}}>Please provide your name to continue</Text>
                </View>
                <View style={{backgroundColor:'white', justifyContent:'center',alignItems:'center',marginTop:50}}>
                    <Image source={require('../img/user.png')} style={{width:400,height:400}}/>
                </View>
                <View style={{marginTop:10}}>
                    <Text>Type your name here</Text>
                    <Input
                    placeholder='Hi! App Community'
                    onChangeText={(text)=>this.setState({name:text})}
                />
                </View>
                
                <View style={{alignItems:'center',flex:1}}>
                    <TouchableOpacity   onPress={()=>this.handlePress()}
                                        style={{paddingHorizontal:100,
                                            width:'90%',
                                            backgroundColor:'#37A936',
                                            height:40,marginBottom:50,
                                            borderRadius:20,alignItems:'center',justifyContent:'center'}}>
                        <Text style={{color:'white',fontSize:16}}>Start Messaging</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}