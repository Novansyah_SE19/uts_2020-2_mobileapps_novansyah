import React, { Component, useEffect } from 'react';
import { ActivityIndicator, Image, KeyboardAvoidingView, LogBox, StatusBar, Text, TextInput, View } from 'react-native';
import { Input } from 'react-native-elements';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/Ionicons';
import AsyncStorage from '@react-native-async-storage/async-storage';

export default class OtpScreen extends Component{
    state={
        first:'',
        second:'',
        third:'',
        fourth:'',
        fifth:'',
        sixth:'',
        confirm: this.props.route.params.confirm,
        number: this.props.route.params.number,
    }

    handlePress(){
        
        const {navigation} = this.props;
        navigation.navigate('StartScreen')
    }

    componentDidMount(){
        LogBox.ignoreLogs(['WARN: ...']);
        console.ignoredYellowBox = true;
    }
    
    confirmCode= async ()=>{
        const code = (this.state.first+this.state.second+this.state.third+this.state.fourth+this.state.fifth+this.state.sixth);
        console.log(this.state.confirm)
        console.log(code)
        try{
            await this.state.confirm.confirm(code)
            .then(async (res)=>{
                console.log(res)
                await AsyncStorage.setItem('data',JSON.stringify(res))
            });
            this.handlePress();
        }
        catch(err){
            alert(JSON.stringify(err))
        }
    }

    render(){
        return(
            <View style={{flex:1,backgroundColor:'white'}}>
                <ActivityIndicator/>
                <StatusBar barStyle="default"></StatusBar>

                <View style={{alignItems:'center',marginTop:50, justifyContent:'center'}}>
                    <Text style={{fontWeight:'bold',fontSize:16}}>Verivication code sent!</Text>
                    <Text style={{fontSize:16,marginTop:10}}>We have sent you an sms with an activation code to</Text>
                    <Text style={{fontSize:16,marginTop:0}}>your number +62 {this.state.number}</Text>
                    {/* <Text style={{fontWeight:'bold',fontSize:16}}>One Time Password</Text> */}
                </View>

                <View style={{flex:1,alignItems:'center',backgroundColor:'white'}}>
                <View style={{width:'70%',marginTop:80,flexDirection:'row'}}>
                <Input
                    maxLength={1}
                    keyboardType="numeric"
                    returnKeyType="next"
                    underlineColorAndroid="#37A936"
                    inputContainerStyle={{borderBottomColor:'white'}}
                    onChangeText={(text)=>{
                                this.setState({first:text});
                                
                                if(text.length>0){

                                    this.secondTextInput.focus();
                                }    
                    }}
                    ref={(input) => { this.FirstTextInput = input; }}
                    containerStyle={{width:50}}
                />
                <Input
                    maxLength={1}
                    keyboardType="numeric"
                    underlineColorAndroid="#37A936"
                    inputContainerStyle={{borderBottomColor:'white'}}
                    onChangeText={(text)=>{
                                this.setState({second:text});
                                
                                if(text.length>0){
                                    this.ThirdTextInput.focus();
                                }    
                    }}
                    onKeyPress={(({ nativeEvent: { key: keyValue }}) =>{
                        if (keyValue === 'Backspace') {
                            this.FirstTextInput.focus();
                        }
                    })}
                    // onTextInput={() => { this.ThirdTextInput.focus(); }}
                    ref={(input) => { this.secondTextInput = input; }}
                    containerStyle={{width:50}}
                />
                <Input
                    maxLength={1}
                    keyboardType="numeric"
                    underlineColorAndroid="#37A936"
                    inputContainerStyle={{borderBottomColor:'white'}}
                    onChangeText={(text)=>{
                                this.setState({third:text});
                                
                                if(text.length>0){
                                    this.FourthTextInput.focus();
                                }    
                    }}
                    onKeyPress={(({ nativeEvent: { key: keyValue }}) =>{
                        if (keyValue === 'Backspace') {
                            this.secondTextInput.focus();
                            this.ThirdTextInput.blur();
                        }
                    })}
                    // onTextInput={() => { this.FourthTextInput.focus(); }}
                    ref={(input) => { this.ThirdTextInput = input; }}
                    containerStyle={{width:50}}
                />
                <Input
                    maxLength={1}
                    keyboardType="numeric"
                    underlineColorAndroid="#37A936"
                    inputContainerStyle={{borderBottomColor:'white'}}
                    onKeyPress={(({ nativeEvent: { key: keyValue }}) =>{
                        if (keyValue === 'Backspace') {
                            this.ThirdTextInput.focus();
                        }
                    })}
                    onChangeText={(text)=>{
                                this.setState({fourth:text});
                                
                                if(text.length>0){

                                    this.FifthTextInput.focus();
                                }    
                    }}
                    ref={(input) => { this.FourthTextInput = input; }}
                    containerStyle={{width:50}}
                />
                <Input
                    maxLength={1}
                    keyboardType="numeric"
                    underlineColorAndroid="#37A936"
                    inputContainerStyle={{borderBottomColor:'white'}}
                    onKeyPress={(({ nativeEvent: { key: keyValue }}) =>{
                        if (keyValue === 'Backspace') {
                            this.FourthTextInput.focus();
                        }
                    })}
                    onChangeText={(text)=>{
                                this.setState({fifth:text});
                                
                                if(text.length>0){

                                    this.sixthTextInput.focus();
                                }    
                    }}
                    ref={(input) => { this.FifthTextInput = input; }}
                    containerStyle={{width:50}}
                />
                <Input
                    maxLength={1}
                    keyboardType="numeric"
                    underlineColorAndroid="#37A936"
                    inputContainerStyle={{borderBottomColor:'white'}}
                    onChangeText={(text)=>{
                                this.setState({sixth:text});
                                
                                if(text.length>0){

                                    this.sixthTextInput.focus();
                                }    
                    }}
                    onKeyPress={(({ nativeEvent: { key: keyValue }}) =>{
                        if (keyValue === 'Backspace') {
                            this.FifthTextInput.focus();
                        }
                    })}
                    ref={(input) => { this.sixthTextInput = input; }}
                    containerStyle={{width:50}}
                />
                </View>
                <View>
                    <Text>Enter 6 digit code</Text>
                </View>
                <View style={{flexDirection:'row',marginTop:50}}>
                    <Text>Resend in </Text>
                    <Text style={{color:'#37A936'}}>01:56</Text>
                </View>
                </View>

                <KeyboardAvoidingView style={{alignItems:'center',flex:1}}>
                    <TouchableOpacity   onPress={()=>this.confirmCode()}
                                        style={{paddingHorizontal:100,
                                            backgroundColor:'#37A936',
                                            height:40,marginBottom:50,
                                            borderRadius:20,alignItems:'center',justifyContent:'center'}}>
                        <Text style={{color:'white',fontSize:16}}>Verify</Text>
                    </TouchableOpacity>
                </KeyboardAvoidingView>
            </View>
        )
    }
}