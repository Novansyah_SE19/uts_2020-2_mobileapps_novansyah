import React, { Component, useEffect } from 'react';
import { ActivityIndicator, Image, StatusBar, Text, Touchable, View } from 'react-native';

export default class SplashScreen extends Component{

    handlePress(){
        const {navigation} = this.props;

        navigation.navigate('WelcomeScreen');
    }
    render(){
        return(
            <View style={{flex:1,backgroundColor:'#FFFFFF'}}>
                <ActivityIndicator/>
                <StatusBar barStyle="default"></StatusBar>

                <View style={{flex:1,alignItems:'center',backgroundColor:'white'}} onTouchStart={()=>this.handlePress()}>
                    <Image source={require('../img/logohiapp.png')} style={{marginTop:250, width:200,height:172}}/>
                    <Text style={{fontSize:40,color:'#37A936',fontWeight:'bold',marginTop:10}}>App</Text>
                </View>

                <View style={{alignItems:'center'}}>
                    <Text>Tap the screen to register</Text>
                </View>
            </View>
        )
    }
}